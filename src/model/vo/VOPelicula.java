package model.vo;

import model.data_structures.ILista;

public class VOPelicula  implements Comparable<VOPelicula>{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;
	private int totalRating;
	private int criteria;
	
	
	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;
	
	
	
	public long getIdUsuario() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public int compareTo(VOPelicula p) {
		if(criteria == 0)
		{
			 if (p.getAgnoPublicacion()<agnoPublicacion)
			 {
				 return 1;
			 }
			 if (p.getAgnoPublicacion()==agnoPublicacion)
			 {
				 return 0;
			 }
			 if (p.getAgnoPublicacion()>agnoPublicacion)
			 {
				 return -1;
			 }
		}
		if (criteria ==1)
		{
			return titulo.compareTo(p.getTitulo());
		}
		
		if(criteria == 2)
		{
			 if (p.getPromedioRatings()<promedioRatings)
			 {
				 return 1;
			 }
			 if (p.getPromedioRatings()==promedioRatings)
			 {
				 return 0;
			 }
			 if (p.getPromedioRatings()==promedioRatings)
			 {
				 return -1;
			 }
			 
		
		}
		
		return 0;
	}
	public int getNumeroTotalRating()
	{
		return totalRating;
	}
	public void setNumeroTotalRating(int p)
	{
		this.totalRating = p;
	}
	public void setCriteria(int i) {
		// TODO Auto-generated method stub
		criteria = i;
	}

}
