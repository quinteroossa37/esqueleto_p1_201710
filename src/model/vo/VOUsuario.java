package model.vo;

public class VOUsuario implements Comparable<VOUsuario> {

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private int criterial;
	private double diferenciaOpinion;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	public int compareTo(VOUsuario arg0) {
		// TODO Auto-generated method stub
		if(criterial == 1){
			if(idUsuario>arg0.getIdUsuario()){
				return 1;
			}
			if(idUsuario<arg0.getIdUsuario()){
				return -1;
			}
			else{
				return 0;
			}
		}
		if(criterial == 2){
			if(primerTimestamp>arg0.getPrimerTimestamp()){
				return 1;
			}
			if(primerTimestamp<arg0.getPrimerTimestamp()){
				return -1;
			}
			else{
				return 0;
			}
		}
		return 0;
	}
	public int getCriterial() {
		return criterial;
	}
	public void setCriterial(int criterial) {
		this.criterial = criterial;
	}
	
}
