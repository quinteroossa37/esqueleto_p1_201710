package model.vo;

public class VOUsuarioConteo implements Comparable<VOUsuarioConteo>{
	private long idUsuario;
	private int conteo;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	public int compareTo(VOUsuarioConteo aComparar) {
		// TODO Auto-generated method stub
		if(conteo>aComparar.conteo){
			return 1;
		}
		if(conteo<aComparar.conteo){
			return -1;
		}
		return 0;
	}
	
}
