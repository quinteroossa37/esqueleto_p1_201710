package model.vo;

import model.data_structures.ILista;
import model.data_structures.MyList;

public class VOGeneroPelicula {
	
	private String genero;
	private int criterial;
	private ILista<VOPelicula> peliculas = new MyList<VOPelicula>();

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public ILista<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ILista<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 
	public void setCriterial(int p)
	{
		for (int i=0; i < peliculas.darNumeroElementos();i++)
		{
			peliculas.darElemento(i).setCriteria(p);
		}
	}

	public int getCriterial() {
		return criterial;
	}
	
}
