package model.data_structures;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import controller.Controlador;
import api.ISistemaRecomendacionPeliculas;
import model.logic.MergeSort;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas 
{


	private MyList <VOPelicula>peliculas = new MyList();
	public MyList<VOPelicula> getPeliculas() {
		return peliculas;
	}


	public void setPeliculas(MyList<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	}


	public MyList<VOUsuario> getUsuarios() {
		return usuarios;
	}


	public void setUsuarios(MyList<VOUsuario> usuarios) {
		this.usuarios = usuarios;
	}


	public MyList getTags() {
		return tags;
	}


	public void setTags(MyList tags) {
		this.tags = tags;
	}


	public MyList<VORating> getRating() {
		return rating;
	}


	public void setRating(MyList<VORating> rating) {
		this.rating = rating;
	}
	private MyList<VOUsuario> usuarios = new MyList();
	private MyList tags = new MyList();
	private MyList generoUsuario = new MyList();
	private MyList <VORating>rating = new MyList();
	private MyList<VOUsuarioGenero> usuarioGenero = new MyList();
	private MyList<VOGeneroPelicula> generosPelicula = new MyList();
	private MyList<VOPeliculaUsuario> peliculaUsuario = new MyList();

	public boolean cargarPeliculasSR(String rutaArchivo)
	{
		try
		{
			FileReader fr= new FileReader(new File(rutaArchivo));
			BufferedReader br = new BufferedReader(fr);
			MyList<String> temp = null;
			br.readLine();
			String linea=br.readLine();
			Pattern pattern=Pattern.compile("\\(([^)]+)\\)");
			String[] datos=null;
			String titulo=null;
			while(linea!=null)
			{
				datos=linea.split(",");
				int op=2;
				titulo=datos[1];
				while(op<datos.length-1)
				{
					titulo+=","+datos[op];
					op++;
				}
				//-------
				int anho=0;
				Matcher m = pattern.matcher(titulo);
				while(m.find()) {

					if(isNumeric(m.group(1).split("-")[0]))
					{
						if(m.group(1).split("-").length!=1)
						{
							if(isNumeric(m.group().split("-")[1]) || m.group().split("-")[1].equals(""))
							{
								anho=Integer.parseInt(m.group(1).split("-")[0]);
								titulo=titulo.replaceAll("\\("+m.group(1)+"\\)"," ");
								break;
							}
						}
						else
						{
							anho=Integer.parseInt(m.group(1).split("-")[0]);
							titulo=titulo.replaceAll("\\("+m.group(1)+"\\)"," ");
							break;
						}

					}
				}
				//--------------------
				if(isNumeric(datos[op]))
				{
					throw new Exception();
				}
				String[] generos=datos[op].split("\\|");
				MyList<String> tem = new MyList() ;
				for(String t:generos)
				{
					tem.agregarElementoFinal(t);
				}
				VOPelicula x = new VOPelicula();
				x.setIdUsuario(Integer.parseInt(datos[0]));
				x.setAgnoPublicacion(anho);
				x.setGenerosAsociados(tem);
				x.setTitulo(titulo);
				x.setTagsAsociados(new MyList<String>());
				if(x!=null)
				{
					peliculas.agregarElementoFinal(x);
					for (int i = 0; i<tem.darNumeroElementos();i++)
					{
						VOGeneroPelicula genero = new VOGeneroPelicula();
						genero.setGenero(tem.darElemento(i));
						boolean hay = false;
						for (int j=0; j<generosPelicula.darNumeroElementos()&&!hay;j++)
						{
							VOGeneroPelicula genero1 = generosPelicula.darElemento(j);
							if (genero1.getGenero().equals(tem))
							{
								genero1.getPeliculas().agregarElementoFinal(x);
								hay=true;
							}
						}
						if (!hay)
						{
							genero.getPeliculas().agregarElementoFinal(x);;
							generosPelicula.agregarElementoFinal(genero);
							return true;
						}

					}
				}
				linea=br.readLine();
				int k=0;
				k++;
			}
			br.close();
			fr.close();
			System.out.println("TERMINÉ");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return false;
	}


	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		File file = new File (rutaRatings);
		try
		{

			BufferedReader br = new BufferedReader (new FileReader(file));
			String linea;
			try {
				linea = br.readLine();
				linea=br.readLine();
				while(linea!=null)
				{
					String[] parametrosRating =linea.split(",");
					boolean hay =false;
					VOUsuario usuario = new VOUsuario();
					VOUsuario usuario1 = new VOUsuario();
					usuario.setIdUsuario(Integer.parseInt(parametrosRating[0]));
					usuario.setNumRatings(0);
					usuario.setPrimerTimestamp(Integer.parseInt(parametrosRating[3]));
					for (int i=0; i<generosPelicula.darNumeroElementos()&&!hay;i++)
					{
						if (peliculas !=null)
						{
							for (int j=0; j< peliculas.darNumeroElementos()&&peliculas.darElemento(j)!=null;j++)
							{
								if (peliculas.darElemento(j).getIdUsuario() == Integer.parseInt(parametrosRating[1]))
								{

									int nuevoTotal = peliculas.darElemento(j).getNumeroTotalRating()+Integer.parseInt(parametrosRating[2]);
									peliculas.darElemento(j).setNumeroTotalRating(nuevoTotal);
									int nuevoProm = nuevoTotal/(peliculas.darElemento(j).getNumeroRatings()+1);
									peliculas.darElemento(j).setPromedioRatings(nuevoProm);
									peliculas.darElemento(j).setNumeroRatings(1);
								}
							}
						}
						if (usuario1.getIdUsuario()==Integer.parseInt(parametrosRating[0]))
						{
							usuario1.setNumRatings(1);
							hay=true;
						}
					}
					if (!hay)
					{

						usuarios.agregarElementoFinal(usuario);

					}

				}
				linea= br.readLine();

				br.close();
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} 
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		File file = new File (rutaTags);
		try
		{
			BufferedReader br = new BufferedReader (new FileReader(file));
			String linea;
			try {
				linea = br.readLine();
				while(linea!=null)
				{
					String[] parametrosTag =linea.split(",");

					if (peliculas !=null)
					{
						for (int j=0; j< peliculas.darNumeroElementos();j++)
						{
							if (peliculas.darElemento(j).getIdUsuario() == Integer.parseInt(parametrosTag[1]))
							{
								VOTag t = new VOTag();
								t.setTag(parametrosTag[2]);
								t.setTimestamp(Integer.parseInt(parametrosTag[3]));
								MyList tag = (MyList) peliculas.darElemento(j).getTagsAsociados();
								tag.agregarElementoFinal(t);
								peliculas.darElemento(j).setTagsAsociados(tag);
								peliculas.darElemento(j).setNumeroTags(1);
							}
						}
					}

				}


				linea= br.readLine();

				br.close();
			} 
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} 
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}




	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return peliculas.darNumeroElementos();
	}

	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return usuarios.darNumeroElementos();
	}

	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tags.darNumeroElementos();
	}

	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub
		MyList <VOGeneroPelicula> peliculas = new MyList();
		for (int i=0; i< generosPelicula.darNumeroElementos();i++)
		{

			MergeSort merge = new MergeSort();
			generosPelicula.darElemento(i).setCriterial(2);
			MyList <VOPelicula>listaOrdenada = merge.mergeSortDescendente((MyList<VOPelicula>) ((generosPelicula.darElemento(i))).getPeliculas());

			for (int j=0; j <listaOrdenada.darNumeroElementos()&& j <n;j++)
			{
				peliculas.agregarElementoFinal(listaOrdenada.darElemento(j));
			}
		}
		return peliculas;
	}

	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		for (int i=0; i<peliculas.darNumeroElementos(); i++)
		{
			peliculas.darElemento(i).setCriteria(0);
		}
		MyList<VOPelicula> peliculas = new MyList();
		MergeSort merge = new MergeSort();
		MyList listaOrdenada = merge.mergeSortDescendente((MyList) (peliculas));	
		return listaOrdenada;
	}

	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		for (int i=0; i< generosPelicula.darNumeroElementos();i++)
		{
			MergeSort merge = new MergeSort();
			generosPelicula.darElemento(i).setCriterial(2);
			MyList listaOrdenada = merge.mergeSortDescendente((MyList) (generosPelicula.darElemento(i)).getPeliculas());
			peliculas.agregarElementoFinal(listaOrdenada.darElemento(0));

		}
		return peliculas;

	}                        



	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		File archivoR = new File("Data.recomendacion.csv");

		try
		{
			if(!archivoR.exists())
			{
				archivoR.createNewFile();
			}

			PrintWriter contenido = new PrintWriter(archivoR);
			contenido.println("Recomendacion");
			for(int i = 0; i<rating.darNumeroElementos();i++){
				contenido.println(rating.darElemento(i).getIdPelicula());
				contenido.println(rating.darElemento(i).getRating());
				contenido.println("======");

			}

			contenido.close();

		}
		catch(Exception e)
		{
		}

		return null;
	}

	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		VOPelicula pelicula = null;
		MyList <VORating>ratingsp = new MyList();
		MergeSort merge = new MergeSort();
		for (int i=0; i <rating.darNumeroElementos();i++)
		{

			if ((rating.darElemento(i).getIdPelicula())==idPelicula)
			{
				ratingsp.agregarElementoFinal(rating.darElemento(i));
			}
		}
		if (ratingsp.darNumeroElementos()==0)
		{
			return null;
		}
		else
		{
			return merge.mergeSortAscendente(ratingsp);
		}
	}

	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		MyList<VOGeneroUsuario> x = new MyList<VOGeneroUsuario>();
		for(int i = 0; i<generoUsuario.darNumeroElementos(); i++){
			x.agregarElementoFinal(generoUsuario.darElemento(i));
			for(int j = 0; j<n;i++){
				Controlador.mergeSortDescendente((MyList<VOUsuarioConteo>) ((VOGeneroUsuario) generoUsuario.darElemento(i)).getUsuarios());
				MyList<VOUsuario> y = new MyList<VOUsuario>();
				y.agregarElementoFinal(((VOGeneroUsuario) generoUsuario.darElemento(i)).getUsuarios().darElemento(j));
				x.darElemento(i).setUsuarios((ILista<VOUsuarioConteo>) y);
			}
		}


		return x;



	}

	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		MyList<VOUsuario> l = new MyList<VOUsuario>();
		for(int i = 0; i<usuarios.darNumeroElementos();i++){
			usuarios.darElemento(i).setCriterial(1);
		}
		l = Controlador.mergeSortAscendente(usuarios);//identificador
		for(int i = 0; i<usuarios.darNumeroElementos();i++){
			usuarios.darElemento(i).setCriterial(2);
		}
		l = Controlador.mergeSortDescendente(usuarios);//tiempo del primer rating
		// como merge es estable, mantiene el orden de identificador a usuarios
		// con es mismo tiempo de primer rating.


		return l;
	}

	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
		MyList<VOGeneroPelicula> x = new MyList<VOGeneroPelicula>();
		for(int i = 0; i<generosPelicula.darNumeroElementos();i++){
			MyList<VOPelicula> l = new MyList<VOPelicula>();
			MyList<VOPelicula> y = new MyList<VOPelicula>();
			l = (MyList<VOPelicula>) generosPelicula.darElemento(i).getPeliculas();
			l = Controlador.mergeSortDescendente(l);
			for(int j = 0; j<n;j++){
				y.agregarElementoFinal(l.darElemento(j));
			}
			VOGeneroPelicula h = new VOGeneroPelicula();
			h.setPeliculas(l);
			h.setGenero(generosPelicula.darElemento(i).getGenero());
			x.agregarElementoFinal(h);
		}

		return x;
	}

	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		MyList<VOTag> x = new MyList<VOTag>();
		MyList<VOGeneroTag> y = new MyList<VOGeneroTag>();
		MyList<VOUsuarioGenero> z = new MyList<VOUsuarioGenero>();
		for(int i = 0; i<usuarioGenero.darNumeroElementos();i++){
			for(int j = 0; j<usuarioGenero.darElemento(i).getListaGeneroTags().darNumeroElementos();j++){
				x = Controlador.mergeSortAscendente((MyList) usuarioGenero.darElemento(i).getListaGeneroTags().darElemento(j).getTags());
				VOGeneroTag generoTag = new VOGeneroTag();
				generoTag.setGenero(usuarioGenero.darElemento(i).getListaGeneroTags().darElemento(j).getGenero());
				generoTag.setTags(x);
				y.agregarElementoFinal(generoTag);
			}
			VOUsuarioGenero p = new VOUsuarioGenero();
			p.setListaGeneroTags(y);
			p.setIdUsuario(usuarioGenero.darElemento(i).getIdUsuario());
			z.agregarElementoFinal(p);
		}

		return z;
	}

	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub

		File file = new File (rutaRecomendacion);
		MyList<VOPeliculaUsuario> r = new MyList<VOPeliculaUsuario>();
		Stack<String> idPelicula = new Stack<String>();
		Stack<String> rating = new Stack<String>();
		try 
		{
			BufferedReader br = new BufferedReader (new FileReader(file));
			String linea;
			try {
				linea = br.readLine();
				while(linea!=null)
				{
					String[] parametrosPeliculas =linea.split(",");
					String x = parametrosPeliculas[0];
					idPelicula.push(x);
					String z = parametrosPeliculas[1];
					rating.push(z);

				}
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} 
		catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while(idPelicula.isEmpty()){
			String h = idPelicula.pop();
			long w = Long.valueOf(h);
			MyList<VOUsuario> q = new MyList();
			VOPeliculaUsuario y = new VOPeliculaUsuario();
			for( int i = 0; i<peliculaUsuario.darNumeroElementos();i++){
				q.agregarElementoFinal(peliculaUsuario.darElemento(i).getUsuariosRecomendados());    
			}
			y.setIdPelicula(w);
			y.setUsuariosRecomendados(q);
			r.agregarElementoFinal(y);
		}
		return r;
	}
	public void crearRecomendacion(){
		File archivoR = new File("Data.recomendacion.csv");

		try
		{
			if(!archivoR.exists())
			{
				archivoR.createNewFile();
			}

			PrintWriter contenido = new PrintWriter(archivoR);
			contenido.println("Recomendacion");
			for(int i = 0; i<rating.darNumeroElementos();i++){
				contenido.println(rating.darElemento(i).getIdPelicula());
				contenido.println(rating.darElemento(i).getRating());
				contenido.println(rating.darElemento(i).getIdUsuario());
				contenido.println("======");

			}

			contenido.close();

		}
		catch(Exception e)
		{
		}

	}

	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		long tiempoInicio = System.currentTimeMillis();

		MyList<VOTag> x = new MyList<VOTag>();
		for( int i = 0; i<peliculas.darNumeroElementos(); i++){
			if(peliculas.darElemento(i).getIdUsuario() == idPelicula){
				x = (MyList<VOTag>) peliculas.darElemento(i).getTagsAsociados();
			}
		}
		x = Controlador.mergeSortDescendente(x);
		long totalTiempo = System.currentTimeMillis() - tiempoInicio;
		agregarOperacionSR("tagsPeliculasSR",tiempoInicio,totalTiempo);
		return x;

	}

	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub

	}

	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		return null;
	}

	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub

	}

	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub

	}

	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub

	}
	private boolean isNumeric(String string) {
		// TODO Auto-generated method stub
		{
			boolean ret = true;
			try {

				Double.parseDouble(string);

			}catch (NumberFormatException e) {
				ret = false;
			}
			return ret;
		}
	}


}
