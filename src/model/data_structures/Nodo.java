package model.data_structures;

public class Nodo <T> {
	private T item;
	private Nodo <T> next;
	
	public void setNodo(T pItem)
	{
		item = pItem;
	}
	
	public void setNext (Nodo <T> pNext)
	{
		next = pNext;
	}
	
	public T getNodo()
	{
		return item;
	}
	
	public Nodo <T> getNext()
	{
		return next;
	}
	
	
}
