package model.data_structures;

import java.util.Iterator;

public class MyList<T> implements ILista  {

	private Nodo <T> primero;
	private int tam = 0;

	public MyList()
	{
		primero = null;
		tam=0;
	}

	public void agregarElementoFinal(Object elem) 
	{
		// TODO Auto-generated method stub
		Nodo<T> actual = primero;
		Nodo<T> nuevo = new Nodo<T>();
		nuevo.setNodo((T) elem);
		if (primero==null)
		{
			primero = nuevo;
			tam++;
			return;
		}
		while (actual.getNext()!=null)
		{
			actual = actual.getNext();
		}
		actual.setNext(nuevo);
		tam++;


	}

	public Nodo<T> darNodoNumero(int pos) {
		// TODO Auto-generated method stub

		T encontrado=null;
		Nodo<T> actual = primero;
		if (primero == null)
		{
			return null;
		}
		else 
		{
			for (int i = 0; (i<tam)&&(encontrado!=null);i++)
			{
				if (i==pos){
					encontrado=(T) actual;
				}
				actual = actual.getNext();
			}
			return (Nodo<T>) encontrado;
		}

	}
	
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int contador = 0;
		Nodo<T> actual = new Nodo<T>();
		actual = primero;
		while(contador<=pos){
			actual = actual.getNext();
			contador++;
		}
		if(actual==null) return null;
		else return actual.getNodo();
	}

	public Object eliminarElemento(int pos) {
		// TODO Auto-generated method stub
		Nodo<T> anterior = null;
		Nodo <T> eliminado=null;
		Nodo <T> actual=null;
		int i =0;
		if (pos ==0){
			eliminado = primero;
			primero = primero.getNext();
			tam--;
			return eliminado;
		}

		actual = primero;
		while ((i<pos)&& (actual!=null)){
			anterior = actual;
			actual=actual.getNext();
			i++;
		}
		anterior.setNext(actual.getNext());
		tam--;
		return actual;

	}

	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return tam;
	}


	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new Iterador() ;
	}
	private class Iterador implements Iterator
	{
		private Nodo actual = primero;

		public boolean hasNext() {
			// TODO Auto-generated method stub
			if ((actual == null)||(actual.getNext()==null))
			{
				return false;
			}
				
			else if (actual.getNext()!=null)
			{
				return true;
			}
			return false;
				
		
		}

		public Object next() {
			// TODO Auto-generated method stub
			return null;
		}

	}
}
