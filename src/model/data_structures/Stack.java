package model.data_structures;

public class Stack<T> {

	private int size;
	private NodoSencillo<T> primero;

	public Stack(){
		size = 0;
		primero = null;
	}
	public void push(T item){
		if(primero == null){
			NodoSencillo<T> a = new NodoSencillo<T>();
			a.setNodo(item);
			primero = a;
			size = 1;
			return;
		}
		NodoSencillo<T> x = new NodoSencillo<T>();
		x.setNodo(item);
		x.setSiguiente(primero);
		size ++;
		primero = x;

	}
	public T pop(){
		NodoSencillo<T> x = new NodoSencillo<T>();
		T item = null;
		if(!this.isEmpty()){
			x = primero.getSiguiente();
			item = primero.getNodo();
			primero = x;
			size --;
		}
		return item;
	}
	public boolean isEmpty(){
		return size()==0;

	}
	public int size(){
		return size;
	}

}

