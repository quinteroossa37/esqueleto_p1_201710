package model.logic;


import model.data_structures.ILista;
import model.data_structures.MyList;
import model.vo.VOPelicula;

public class MergeSort<T extends Comparable<T>> {

	public MyList<T> mergeSortAscendente(MyList<T> desorganizada){
		MyList<T> ordenada = new MyList<T>();
		if(desorganizada.darNumeroElementos()> 1){
			int i;
			int izqT = desorganizada.darNumeroElementos()/2;
			int derT = desorganizada.darNumeroElementos()-izqT;
			MyList<T> izq = new MyList<T>();
			MyList<T> der =new MyList<T>();
			for(i = 0; i<izqT;i++){
				izq.agregarElementoFinal(desorganizada.darElemento(i));
			}
			for(i = izqT;i<izqT+derT; i++){
				der.agregarElementoFinal(desorganizada.darElemento(i));
			}
			izq = mergeSortAscendente(izq);
			der = mergeSortAscendente(der);
			int k = 0;
			int l = 0;
			MyList<T> organizando = new MyList<T>();
			while(izq.darNumeroElementos()!=k && der.darNumeroElementos()!=l){
				if(((T) izq.darElemento(k)).compareTo((T) der.darElemento(l))<0){
					organizando.agregarElementoFinal(izq.darElemento(k));	
					k++; 
				}
				else{
					organizando.agregarElementoFinal(der.darElemento(l));
					l++;
				}
			}
			while(izq.darNumeroElementos()!=k){
				ordenada.agregarElementoFinal(izq.darElemento(k)); 
				k++;
			}
			while(der.darNumeroElementos()!=l){
				ordenada.agregarElementoFinal(der.darElemento(l));
				l++;
			}
		}
		return ordenada;
	}
	public MyList<T> mergeSortDescendente(MyList<T> desorganizada){
		MyList<T> ordenada = new MyList<T>();
		if(desorganizada.darNumeroElementos()> 1){
			int i;
			int izqT = desorganizada.darNumeroElementos()/2;
			int derT = desorganizada.darNumeroElementos()-izqT;
			MyList<T> izq = new MyList<T>();
			MyList<T> der =new MyList<T>();
			for(i = 0; i<izqT;i++){
				izq.agregarElementoFinal(desorganizada.darElemento(i));
			}
			for(i = izqT;i<izqT+derT; i++){
				der.agregarElementoFinal(desorganizada.darElemento(i));
			}
			izq = mergeSortDescendente(izq);
			der = mergeSortDescendente(der);
			int k = 0;
			int l = 0;
			MyList<T> organizando = new MyList<T>();
			while(izq.darNumeroElementos()!=k && der.darNumeroElementos()!=l){
				if(((T) izq.darElemento(k)).compareTo((T) der.darElemento(l))>0){
					organizando.agregarElementoFinal(izq.darElemento(k));	
					k++; 
				}
				else{
					organizando.agregarElementoFinal(der.darElemento(l));
					l++;
				}
			}
			while(izq.darNumeroElementos()!=k){
				ordenada.agregarElementoFinal(izq.darElemento(k)); 
				k++;
			}
			while(der.darNumeroElementos()!=l){
				ordenada.agregarElementoFinal(der.darElemento(l));
				l++;
			}
		}
		return ordenada;
	}
}
