package controller;

import model.data_structures.MyList;
import model.logic.MergeSort;

public class Controlador {
	
	private static MergeSort organizador= new MergeSort();


	public static MyList mergeSortAscendente(MyList desorganizada){
		return organizador.mergeSortAscendente(desorganizada);
	}
	
	public static MyList mergeSortDescendente(MyList desorganizada){
		return organizador.mergeSortDescendente(desorganizada);
	}
}
