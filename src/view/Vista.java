package view;

import java.util.Scanner;


import model.data_structures.SistemaRecomendacionPeliculas;

public class Vista {

	private static SistemaRecomendacionPeliculas SPR = new SistemaRecomendacionPeliculas();

	public final static String ARCHIVO_CARGARPELICULAS = "./Data/movies.csv";
	public final static String ARCHIVO_CARGARRATINGS = "./Data/ratings.csv";
	public final static String ARCHIVO_CARGARTAGS = "./Data/tags.csv";

	public static void main(String[] args) {




		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();

			int option = sc.nextInt();

			switch(option){
			case 1:

				SPR.cargarPeliculasSR(ARCHIVO_CARGARPELICULAS);

				SPR.cargarRatingsSR(ARCHIVO_CARGARRATINGS);

				SPR.cargarTagsSR(ARCHIVO_CARGARTAGS);

				break;
			case 2:
				String ap = sc.next();
				
				System.out.println(SPR.peliculasPopularesSR(Integer.valueOf(ap)));


				break;

			case 3:
				System.out.println(SPR.catalogoPeliculasOrdenadoSR());


				break;
			case 4:
				System.out.println(SPR.recomendarGeneroSR());


				break;
			case 5:
				System.out.println(SPR.opinionRatingsGeneroSR());


				break;
			
			case 6:
				System.out.println("Ingrese Expresion:");
				String e = sc.next();

			case 7:
				System.out.println("Ingrese numero de objetos:");
				String x = sc.next();
				System.out.println(SPR.usuariosActivosSR(Integer.valueOf(x)));
			
			case 8:
				System.out.println(SPR.catalogoUsuariosOrdenadoSR());
				
			case 9:
				System.out.println("Ingrese numero de objetos:");
				String g = sc.next();
				System.out.println(SPR.recomendarTagsGeneroSR(Integer.valueOf(g)));
				
			case 10:
				System.out.println(SPR.catalogoUsuariosOrdenadoSR());
				
			case 11:
				
				System.out.println(SPR.opinionTagsGeneroSR());
				
			case 12:
				System.out.println("Ingrese la ruta donde quiere escribir el archivo y la cantidad de objetos que desea imprimir separados por (,)");
				String d = sc.next();
				String[] f = d.split(",");
				System.out.println(SPR.recomendarUsuariosSR(f[0], Integer.valueOf(f[1])));
				
			case 13:
				System.out.println("Ingrese cantidad de objetos");
				String l = sc.next();
				System.out.println(SPR.tagsPeliculaSR(Integer.valueOf(l)));
				
				
			case 20:	
				fin=true;
				break;

			}



		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 1----------------------");
		System.out.println("1. Cargar Todo");
		System.out.println("2. A1");
		System.out.println("3. A2");
		System.out.println("4. A3");
		System.out.println("5. A4");
		System.out.println("6. A5");
		System.out.println("7. A6");
		System.out.println("8. B1");
		System.out.println("9. B2");
		System.out.println("10. B3");
		System.out.println("11. B4");
		System.out.println("12. B5");
		System.out.println("13  B6");
		System.out.println("20  Acabar el programa");
		
	
	}

}
