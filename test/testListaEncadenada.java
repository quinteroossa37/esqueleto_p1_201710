package model.data_estructures.test;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class testListaEncadenada extends TestCase{
	private ListaEncadenada<String> listaEncadenada;

	public void setupEscenario1()
	{
		listaEncadenada=new ListaEncadenada<String>();
	}
	public void setupEscenario2()
	{
		listaEncadenada=new ListaEncadenada<String>();
		listaEncadenada.agregarElementoFinal("Hola");
		listaEncadenada.agregarElementoFinal("Como");
		listaEncadenada.agregarElementoFinal("Va");
		listaEncadenada.agregarElementoFinal("S");
	}
	public void testAgregarElemento()
	{
		setupEscenario1();
		listaEncadenada.agregarElementoFinal("x");
		assertEquals("x", listaEncadenada.darElemento(0));
		listaEncadenada.agregarElementoFinal("y");
		assertEquals("y", listaEncadenada.darElemento(1));
		listaEncadenada.agregarElementoFinal("z");
		assertEquals("z", listaEncadenada.darElemento(2));
	}
	public void testDarElementoPos()
	{
		setupEscenario2();
		assertEquals("Va", listaEncadenada.darElemento(2));
		assertEquals("Hola", listaEncadenada.darElemento(0));
	}
	public void testdarActual()
	{
		setupEscenario2();
		assertEquals("S", listaEncadenada.darElementoPosicionActual());
	}
	public void testRetroceder()
	{
		setupEscenario2();
		listaEncadenada.retrocederPosicionAnterior();
		assertEquals("Va", listaEncadenada.darElementoPosicionActual());
	}
	public void testAvanzar()
	{
		setupEscenario2();
		listaEncadenada.retrocederPosicionAnterior();
		listaEncadenada.retrocederPosicionAnterior();
		listaEncadenada.retrocederPosicionAnterior();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("Como", listaEncadenada.darElementoPosicionActual());
	}
	public void testDarNumerElementos()
	{
		setupEscenario1();
		assertEquals(0, listaEncadenada.darNumeroElementos());
		setupEscenario2();
		assertEquals(4, listaEncadenada.darNumeroElementos());
	}
}
